package francois.rabanel.tp8;

import static androidx.core.location.LocationManagerCompat.registerGnssStatusCallback;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.GnssStatus;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.util.Range;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity implements LocationListener {
    static String TAG = "MainActivity";
    boolean isLoaded = false;
    long startTime;
    long endTime;
    LinearLayout gpsStatusLayout, positionLayout, satellitesLayout, addressLayout;
    LocationManager locationManager;
    GnssStatus.Callback mGnssStatusCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        startTime = System.currentTimeMillis();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // J'ai ajouté dans le xml tout les blocs LinearLayout que je souhaite manipuler et
        // je vais ajouter des textView directement dans le code ce sera plus simple.
        gpsStatusLayout = (LinearLayout) findViewById(R.id.gps_status_layout);
        positionLayout = (LinearLayout) findViewById(R.id.position_layout);
        satellitesLayout = (LinearLayout) findViewById(R.id.satellites_layout);
        addressLayout = (LinearLayout) findViewById(R.id.address_layout);
        Button testNetworkButton = (Button) findViewById(R.id.test_network_btn);


        // Ask to use and initialize GPS infos.
        initGPS();

       // Callback called when location has changed.
       mGnssStatusCallback = new GnssStatus.Callback() {
            @Override
            public void onStarted() {
                super.onStarted();
                Log.d(TAG, "------------------on started callback");
            }

            @Override
            public void onStopped() {
                super.onStopped();
                Log.d(TAG, "------------------on stopped callback");
            }

            @Override
            public void onFirstFix(int ttffMillis) {
                super.onFirstFix(ttffMillis);
                endTime = System.currentTimeMillis();
                Log.d(TAG, "------------------on first fix callback");

                // Calculate the difference with a time between onCreate and onFirstFix.
                if (!isLoaded) {
                    addViewToLayout(
                            gpsStatusLayout,
                            "Time required to receive the first fix: " +
                                    (endTime - startTime) +
                                    "ms"
                    );
                    isLoaded = true;
                }
            }

            @Override
            public void onSatelliteStatusChanged(@NonNull GnssStatus status) {
                super.onSatelliteStatusChanged(status);
                Log.d(TAG, "------------------on satellite status changed callback");
                resetViewsFor(satellitesLayout);
                getSatellitesDetails(status);
            }
        };

       // Move to Q3 Intent with Network
        testNetworkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NetworkActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onResume() {
        super.onResume();
        locationManager.registerGnssStatusCallback(mGnssStatusCallback);
    }

    @Override
    protected void onStop() {
        super.onStop();
       locationManager.removeUpdates(this);
       locationManager.unregisterGnssStatusCallback(mGnssStatusCallback);
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        resetViewsFor(positionLayout);
        resetViewsFor(addressLayout);

        getPositionDetails(location);
        getAddressDetails(location);
    }

    private void initGPS(){
        // On demande l'autorisation d'utiliser le GPS.
        if (ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, 100);
        }

        // On initialise le LocationManager pour manipuler les données de géolocalisations.
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // Une fois fait on peut déjà ajouter à notre vue si le GPS est disponible.
        addViewToLayout(
                gpsStatusLayout,
                "GPS " + hasGPSEnabled(
                        locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                )
        );

        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, 30000, 5, this
        );
    }
    private String hasGPSEnabled(boolean status){
        if(status){
            return new String("OK");
        } else {
            return new String("NOT OK");
        }
    }
    private void resetViewsFor(LinearLayout layout) {
        // Reset la vue en cas de refresh
        layout.removeAllViews();
    }
    private void addViewToLayout(LinearLayout layout, String text) {
        TextView textView = new TextView(this);
        textView.setText(text);
        layout.addView(textView);
    }
    private void getPositionDetails(Location location) {
        addViewToLayout(positionLayout, "AVAILABLE");
        addViewToLayout(positionLayout, "Longitude : " + location.getLongitude());
        addViewToLayout(positionLayout, "Latitude : " + location.getLatitude());
        addViewToLayout(positionLayout, "Altitude : " + location.getAltitude() + " m");
        addViewToLayout(positionLayout, "Accuracy : " + location.getAccuracy() + " m");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            addViewToLayout(
                    positionLayout,
                    "Speed : " + location.getSpeedAccuracyMetersPerSecond() + " m/s"
            );
        }

        addViewToLayout(positionLayout, "Bearing : " + location.getBearing() + " deg");

        // Format Unix Epoch time to a lovely string.
        Date date = new Date(location.getTime());
        SimpleDateFormat dateFormatter = new SimpleDateFormat(
                "EEEE dd MMMM yyyy - HH:mm:ss", Locale.FRANCE
        );

        // I've changed the timezone to fit with CEST (HAEC) like picture in the first question.
        dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC+2"));
        String dateFormatted = dateFormatter.format(date);

        addViewToLayout(positionLayout, "Time : " + dateFormatted + " HAEC");
    }
    private void getSatellitesDetails(GnssStatus status){
        Integer maxRange = status.getSatelliteCount();
        Integer minRange = 0;

        for (int i = minRange; i < maxRange; i++){

            String satelliteId = new String("Satellite " + status.getSvid(i));
            String azimuthDegrees = new String("Az: " + status.getAzimuthDegrees(i) + " deg");
            String elevationDegrees = new String("El " + status.getElevationDegrees(i));
            String signalToNoiseRation = "";
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                signalToNoiseRation = new String("Snr " + status.getCarrierFrequencyHz(i));
            }
            String almanacData = new String("Almanac: " + status.hasAlmanacData(i));
            String ephemeridsData = new String("Ephemeris: " + status.hasEphemerisData(i));
            String usedInFix = new String("UsedInFix: " + status.usedInFix(i));

            String firstLineDetailsSatellite = new String(azimuthDegrees + ", " + elevationDegrees + ", " + signalToNoiseRation);
            String secondLineDetailsSatellite = new String(almanacData + ", " + ephemeridsData + ", " + usedInFix);


            addViewToLayout(satellitesLayout, satelliteId);
            addViewToLayout(satellitesLayout, firstLineDetailsSatellite);
            addViewToLayout(satellitesLayout, secondLineDetailsSatellite);

        }
    }
    private void getAddressDetails(Location location){
        // Retrieve the address.
        Geocoder geocoder = new Geocoder(MainActivity.this, Locale.FRANCE);
        try {
            List<Address> addresses = geocoder.getFromLocation(
                    location.getLatitude(), location.getLongitude(), 1
            );
            String address = addresses.get(0).getAddressLine(0);

            addViewToLayout(addressLayout, "Address:");
            addViewToLayout(addressLayout, address);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}