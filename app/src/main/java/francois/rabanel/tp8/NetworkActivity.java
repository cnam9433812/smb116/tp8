package francois.rabanel.tp8;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class NetworkActivity extends AppCompatActivity implements LocationListener {
    static String TAG = "NetworkActivity";
    LinearLayout networkAddressLayout, networkPositionLayout, networkStatusLayout;
    LocationManager locationManager;
    Location location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network);
        networkStatusLayout = (LinearLayout) findViewById(R.id.network_status_layout);
        networkPositionLayout = (LinearLayout) findViewById(R.id.network_position_layout);
        networkAddressLayout = (LinearLayout) findViewById(R.id.network_address_layout);

        initNetwork();
    }
    @Override
    public void onLocationChanged(@NonNull Location location) {
        resetViewsFor(networkPositionLayout);
        resetViewsFor(networkAddressLayout);

        Log.d(TAG, "location changed: " + location);

        getPositionDetails(location);
        getAddressDetails(location);
    }
    private void initNetwork(){
        // On demande l'autorisation d'utiliser le GPS.
        if (ActivityCompat.checkSelfPermission(
                this, android.Manifest.permission.ACCESS_FINE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(NetworkActivity.this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, 100);
        }

        // On initialise le LocationManager pour manipuler les données de géolocalisations.
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // Une fois fait on peut déjà ajouter à notre vue si le NETWORK est disponible.
        addViewToLayout(
                networkStatusLayout,
                "NETWORK " + hasNetworkEnabled(
                        locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
                )
        );
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, 10000, 5, this
        );
        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        Log.d(TAG, "last location init: " + location);
    }
    private void resetViewsFor(LinearLayout layout) {
        // Reset la vue en cas de refresh
        layout.removeAllViews();
    }
    private void addViewToLayout(LinearLayout layout, String text) {
        TextView textView = new TextView(this);
        textView.setText(text);
        layout.addView(textView);
    }
    private String hasNetworkEnabled(boolean status){
        if(status){
            return new String("OK");
        } else {
            return new String("NOT OK");
        }
    }
    private void getPositionDetails(Location location) {
        addViewToLayout(networkPositionLayout, "Longitude : " + location.getLongitude());
        addViewToLayout(networkPositionLayout, "Latitude : " + location.getLatitude());
        addViewToLayout(networkPositionLayout, "Altitude : " + location.getAltitude() + " m");
        addViewToLayout(networkPositionLayout, "Accuracy : " + location.getAccuracy() + " m");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            addViewToLayout(
                    networkPositionLayout,
                    "Speed : " + location.getSpeedAccuracyMetersPerSecond() + " m/s"
            );
        }

        addViewToLayout(networkPositionLayout, "Bearing : " + location.getBearing() + " deg");

        // Format Unix Epoch time to a lovely string.
        Date date = new Date(location.getTime());
        SimpleDateFormat dateFormatter = new SimpleDateFormat(
                "EEEE dd MMMM yyyy - HH:mm:ss", Locale.FRANCE
        );

        // I've changed the timezone to fit with CEST (HAEC) like picture in the first question.
        dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC+2"));
        String dateFormatted = dateFormatter.format(date);

        addViewToLayout(networkPositionLayout, "Time : " + dateFormatted + " HAEC");
    }
    private void getAddressDetails(Location location){
        // Retrieve the address.
        Geocoder geocoder = new Geocoder(NetworkActivity.this, Locale.FRANCE);
        try {
            List<Address> addresses = geocoder.getFromLocation(
                    location.getLatitude(), location.getLongitude(), 1
            );
            String address = addresses.get(0).getAddressLine(0);

            addViewToLayout(networkAddressLayout, "Address:");
            addViewToLayout(networkAddressLayout, address);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}